export interface Entry {
  id: number;
  description: string;
  value: string;
  isExpense: boolean;
}
