import { ButtonSaveOrCancels } from '@components/button';
import { Form } from 'semantic-ui-react';

const NewEntry = () => {
  return (
    <Form unstackable onSubmit={() => null}>
      <Form.Group>
        <Form.Input icon='tags' width={12} label='Description' placeholder='new shinny thing' />
        <Form.Input
          width={4}
          label='Value'
          placeholder='100.00'
          icon='dollar'
          iconPosition='left'
        />
      </Form.Group>
      <ButtonSaveOrCancels />
    </Form>
  );
};

export default NewEntry;
