import { SemanticCOLORS } from 'semantic-ui-react/dist/commonjs/generic';
import { StatisticSizeProp } from 'semantic-ui-react/dist/commonjs/views/Statistic/Statistic';

type BalanceProps = {
  color: SemanticCOLORS;
  label: string;
  value: string;
  size: StatisticSizeProp;
};

export default BalanceProps;
