import { Statistic } from 'semantic-ui-react';
import BalanceProps from './balance.type';

const DisplayComponent = ({ color, label, size, value }: BalanceProps) => {
  return (
    <Statistic size={size} color={color}>
      <Statistic.Label style={{ textAlign: 'center' }}>{label}:</Statistic.Label>
      <Statistic.Value>{value}</Statistic.Value>
    </Statistic>
  );
};

export default DisplayComponent;
