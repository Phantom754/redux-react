import { Entry } from '@/types';

export type EntryProps = {
  entries: Entry[];
  deleteEntry: (id: number) => unknown;
};
