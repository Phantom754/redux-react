import { Entry } from '@/types';
import EntryLine from '@components/entryBalance';
import { Container } from 'semantic-ui-react';
import { EntryProps } from './entries.type';

const EntryLines = ({ entries, deleteEntry }: EntryProps) => {
  return (
    <Container>
      {entries.map((entry: Entry, index: number) => (
        <EntryLine key={index} {...entry} deleteEntry={deleteEntry} />
      ))}
    </Container>
  );
};

export default EntryLines;
