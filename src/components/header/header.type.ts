export type HeaderProps = {
  mode: string | undefined | null;
  label: string | undefined | null;
};
