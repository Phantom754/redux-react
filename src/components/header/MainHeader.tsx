import { Header } from 'semantic-ui-react';
import { HeaderProps } from './header.type';

const MainHeader = ({ label, mode }: HeaderProps) => {
  return (
    <div>
      <Header as={mode}>{label}</Header>
    </div>
  );
};

export default MainHeader;
