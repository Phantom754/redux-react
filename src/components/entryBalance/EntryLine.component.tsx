import { Grid, Icon, Segment } from 'semantic-ui-react';
import EntryLineProps from './entryLine.type';

const EntryLineComponent = ({ isExpense, description, value, id, deleteEntry }: EntryLineProps) => {
  return (
    <Segment color={isExpense ? 'green' : 'red'}>
      <Grid columns={3} textAlign='right'>
        <Grid.Row>
          <Grid.Column width={10} textAlign='left'>
            {description}
          </Grid.Column>
          <Grid.Column width={3}>{value}</Grid.Column>
          <Grid.Column width={3}>
            <Icon name='edit' bordered />
            <Icon name='trash' bordered onClick={() => deleteEntry(id)} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  );
};

export default EntryLineComponent;
