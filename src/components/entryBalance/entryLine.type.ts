type EntryLineProps = {
  id: number;
  isExpense: boolean;
  description: string;
  value: string | number;
  deleteEntry: (id: number) => unknown;
};

export default EntryLineProps;
