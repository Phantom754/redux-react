import { Grid, Segment } from 'semantic-ui-react';
import { DisplayBalance } from '../balance';

const DisplaysComponent = () => {
  return (
    <Segment textAlign='center'>
      <Grid columns={2} divided>
        <Grid.Row>
          <Grid.Column>
            <DisplayBalance color='green' label='Income' size='tiny' value='31,045.00' />
          </Grid.Column>
          <Grid.Column>
            <DisplayBalance color='red' label='Expenses' size='tiny' value='1,045.00' />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Segment>
  );
};

export default DisplaysComponent;
