import { Button } from 'semantic-ui-react';

const ButtonSaveOrCancel = () => {
  return (
    <Button.Group>
      <Button onClick={() => console.log(22)}>Cancel</Button>
      <Button.Or />
      <Button onClick={() => console.log(22)} primary>
        OK
      </Button>
    </Button.Group>
  );
};

export default ButtonSaveOrCancel;
