import { Entry } from '@/types';

const initialEntries: Entry[] = [
  {
    id: 2,
    description: 'Work income',
    value: '$1000.00',
    isExpense: false,
  },
  {
    id: 3,
    description: 'Warrer bill',
    value: '$20.00',
    isExpense: true,
  },
  {
    id: 4,
    description: 'Rent',
    value: '$300',
    isExpense: true,
  },
  {
    id: 5,
    description: 'Power Bill',
    isExpense: true,
    value: '$50',
  },
];

export { initialEntries };
