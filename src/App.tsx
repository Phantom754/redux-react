import MainHeader from '@components/header';
import { Container } from 'semantic-ui-react';
import { DisplayBalance } from '@components/balance';
import { DisplaysBalances } from '@components/balanceGroup';
import { NewEntryForm } from '@components/form';
import { useState } from 'react';
import { initialEntries } from '@constants/state';
import { Entry } from './types';
import EntryLines from './components/entryLines';

const App = () => {
  const [entries, setEntries] = useState<Entry[]>(initialEntries);
  const deleteEntry = (id: number) => {
    const result = entries.filter((entry: Entry) => entry.id !== id);
    setEntries(result);
  };
  return (
    <Container>
      <MainHeader label='Hello budget' mode='h1' />
      <DisplayBalance color='black' label='Your balance' size='small' value='42,550.00' />
      <DisplaysBalances />
      <MainHeader mode='h3' label='History' />
      <EntryLines entries={entries} deleteEntry={deleteEntry} />
      <MainHeader mode='h3' label='Add new transaction' />
      <NewEntryForm />
    </Container>
  );
};

export default App;
