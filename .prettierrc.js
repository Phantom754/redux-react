module.exports = {
    jsxSingleQuote: true,
    semi: true,
    trailingComma: 'all',
    singleQuote: true,
    printWidth: 100,
    tabWidth: 2,
    bracketSpacing: true,
    useTabs: false,
    arrowParens: 'always',
    endOfLine: 'auto'
  };
  